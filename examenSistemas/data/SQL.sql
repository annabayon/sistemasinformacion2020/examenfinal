﻿DROP DATABASE IF EXISTS examenFinal;
CREATE DATABASE examenFinal;
USE examenFinal; 

CREATE TABLE ciudades(
  id int,
  nombre varchar(255),
  habitantes float,
  escudo varchar(255),
  mapa varchar(255),
  PRIMARY KEY(id)
  );
